//
//  FriendSearchViewController.swift
//  Makestagram
//
//  Created by 吴梦宇 on 6/26/15.
//  Copyright (c) 2015 Make School. All rights reserved.
//

import UIKit
import ConvenienceKit
import Parse

class FriendSearchViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
// stores all the users that match the current search
    
    var users: [PFUser]?
    
    /*
    This is a local cache. It stores all the users this user is following.
    It is used to update the UI immediately upon user interaction, instead of waiting
    for a server response.
    */
    
    var followingUsers: [PFUser]? {
        didSet {
            
            tableView.reloadData()
            
        }
        
    }
    
    // the current parse query
    
    var query: PFQuery? {
        didSet{
            // whenever we assign a new query, cancel any previous requests
            
            // where is the oldValue declared?
            oldValue?.cancel()
        
        }
        
        
    }
    
    enum State {
        
        case DefaultMode
        case SearchMode
        
    }
    
    var state: State = .DefaultMode {
        
        didSet {
            
            switch (state){
                
            case .DefaultMode:
                query = ParseHelper.allUsers(updateList)
                
            case .SearchMode:
                let searchText = searchBar?.text ?? ""
                query=ParseHelper.searchUsers(searchText, completionBlock: updateList)
                
                
            }
            
            
        }
        
    }
    
    // MARK: Update userlist
    
    /**
    Is called as the completion block of all queries.
    As soon as a query completes, this method updates the Table View.
    */
    
    func updateList(results: [AnyObject]?, error: NSError?){
        
         self.users = results as? [PFUser] ?? []
         self.tableView.reloadData()
        
        if let error = error {
            
            ErrorHandling.defaultErrorHandler(error)
        }
        
        
    }
    
  // MARK: View Lifecycle
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        state = .DefaultMode
        // fill the cache of a user's followees
        
        ParseHelper.getFollowingUsersForUser(PFUser.currentUser()!){(results: [AnyObject]?, error: NSError?) -> Void in
            
            let relations = results as? [PFObject] ?? []
            // use map to extract the user from a follow object
            
            
            // objectForKey  Returns the value associated with a given key.
            self.followingUsers = relations.map{ $0.objectForKey(ParseHelper.ParseFollowToUser) as! PFUser}
            
            if let error = error {
                // Call the default error handler in case of an Error
                ErrorHandling.defaultErrorHandler(error)
            }
            
        }
        
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: TableView Data Source

extension FriendSearchViewController: UITableViewDataSource {
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users?.count ?? 0
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("UserCell") as! FriendSearchTableViewCell
        
        let user=users![indexPath.row]
        
        cell.user = user
        
        if let followingUsers = followingUsers {
            
            // chekc if current user is already following displayed user
            
            cell.canFollow = !contains(followingUsers, user)
        }
        
     // when to set delegate?, when the cell is going to be displayed
    
        cell.delegate = self //set the delegate of friendsearchdelagte to be the viewcontroller
        
       return cell
    }
}

// MARK: Searchbar Delegate

extension FriendSearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
        self.state = State.SearchMode
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder() //hide keyboards
        searchBar.text=""
        searchBar.setShowsCancelButton(false, animated: true)
        
        self.state = State.DefaultMode
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        ParseHelper.searchUsers(searchText, completionBlock: updateList)
    }
}

// MARK: FriendSearchTableViewCell Delegate
extension FriendSearchViewController: FriendSearchTableViewCellDelegate{
    
    //this delegate deal with object stored to server and delete from server
    
    func cell(cell: FriendSearchTableViewCell, didSelectFollowUser user: PFUser){
        ParseHelper.addFollowRelationshipFromUser(PFUser.currentUser()!, toUser: user)
        
        // update local cache
        followingUsers?.append(user)
    }
    
    
    func cell(cell: FriendSearchTableViewCell, didSelectUnfollowUser user: PFUser){
    
        if var followingUsers = followingUsers {
        
        ParseHelper.removeFollowRelationshipFromUser(PFUser.currentUser()!, toUser: user)
        // update local cache
        
        removeObject(user, fromArray: &followingUsers)
        self.followingUsers = followingUsers
            
        }
        
    }
    
}
