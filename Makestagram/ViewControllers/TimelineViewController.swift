//
//  TimelineViewController.swift
//  Makestagram
//
//  Created by 吴梦宇 on 6/26/15.
//  Copyright (c) 2015 Make School. All rights reserved.
//

import UIKit
import Parse
import ConvenienceKit

class TimelineViewController: UIViewController, TimelineComponentTarget {
    
    // implement timelineComponentTarget
    // angled brackets: the type of object you are displaying (Post) and the class that will be the target of the TimelineComponent (that's the TimelineViewController in our case).
    var timelineComponent : TimelineComponent<Post, TimelineViewController>!
    let defaultRange = 0...4
    let additionalRangeSize = 5
    
    
    
    //var posts: [Post]=[]
    var photoTakingHelper: PhotoTakingHelper?

    
    /**
    This method should load the items within the specified range and call the
    `completionBlock`, with the items as argument, upon completion.
    */
    func loadInRange(range: Range<Int>, completionBlock: ([Post]?) -> Void){
        ParseHelper.timelineRequestforCurrentUser(range){ (result: [AnyObject]?, error: NSError?) -> Void in
            let posts = result as? [Post] ?? []
            completionBlock(posts)
            
        }
        
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timelineComponent = TimelineComponent(target: self)

        // Do any additional setup after loading the view.
        
        self.tabBarController?.delegate=self
  
    
    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // parse in a handler
        //        ParseHelper.timelineRequestforCurrentUser{ (result: [AnyObject]?, error: NSError?) -> Void in
        //            self.posts = result as? [Post] ?? []
        //
        //           // We no longer want to download all images immediately after the timeline query completes, instead we want to load them lazily as soon as a post is displayed.
        ////
        ////            for post in self.posts {
        ////                // download the data
        ////                let data = post.imageFile?.getData()
        ////                post.image.value = UIImage(data: data!, scale:1.0) // change to the image
        ////
        ////            }
        //
        //
        //            //importnat: we only downloading metadata of all posts upfront and deferring the image download until a post is displayed
        //
        //          // trigger the image download for our visible posts
        //            
        //            self.tableView.reloadData()
        //        }
        
        
        // only load when content array is empty, but if I added some new stuff? the new posts didn't show up
        timelineComponent.loadInitialIfRequired()
        
    }
    
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: tab bar delegate

extension TimelineViewController: UITabBarControllerDelegate{
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        if(viewController is PhotoViewController) {
            println("Take Photo")
            takePhoto()
            return false
        } else {
            return true
        }
    }
    
    func takePhoto(){
        
        photoTakingHelper=PhotoTakingHelper(viewController: self.tabBarController!){ (image: UIImage?) in
            println("received a callback")
            
            let post = Post()
            post.image.value=image
            
            post.uploadPost()
            
            
            
        
            
        }
    }
}



// MARK: when will these function be called???
extension TimelineViewController: UITableViewDataSource{
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCellWithIdentifier("PostHeader") as! PostSectionHeaderView
        let post=self.timelineComponent.content[section]
        headerCell.post=post
        
        return headerCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return timelineComponent.content.count

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return posts.count
        
        return 1
        
    }
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    
    // this method is called when the tbale view is about to present a cell, immediately before the cell becomes visible
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("PostCell") as! PostTableViewCell
        
    
    
        
        // we want to assign a post to the cell instead of assign image to imageview directly
        //cell.postImageView.image = posts[indexPath.row].image.value
        
       let post = timelineComponent.content[indexPath.section]
        
        // download, only downloaded with needed
        post.downloadImage()
        
          //loading the likes for each post, as soon as it gets displayed.
        post.fetchLikes()
        
        
        cell.post=post
        
        return cell
        
        
    }

}

extension TimelineViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        timelineComponent.targetWillDisplayEntry(indexPath.section)
    }
}
