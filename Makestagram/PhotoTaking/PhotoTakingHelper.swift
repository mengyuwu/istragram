//
//  PhotoTakingHelper.swift
//  Makestagram
//
//  Created by 吴梦宇 on 6/26/15.
//  Copyright (c) 2015 Make School. All rights reserved.
//

import UIKit

//a function of type PhotoTakingHelperCallback takes an UIImage? as parameter and returns Void
// callback functions for returning photos
//Using the typealias keyword we can provide a function signature with a name
//callback is the function reference, usually a fucntion name; func something(){}  self.callback=something


typealias PhotoTakingHelperCallback = UIImage? -> Void

class PhotoTakingHelper: NSObject {
    
    weak var viewController: UIViewController!
    var callback: PhotoTakingHelperCallback
    var imagePickerController: UIImagePickerController?
    
    init(viewController: UIViewController, callback: PhotoTakingHelperCallback){
        self.viewController = viewController
        self.callback = callback
        super.init()

        showPhotoSourceSelection()
    }
    
    
    
    func showPhotoSourceSelection() {
        let alertController = UIAlertController(title: nil, message: "where do you want to get your picture from?", preferredStyle: .ActionSheet)
        
        
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        if (UIImagePickerController.isCameraDeviceAvailable(.Rear)){
            let cameraAction = UIAlertAction(title: "Photo from Camera", style: .Default){ (action) in
             self.showImagePickerController(.Camera)
            }
            
            alertController.addAction(cameraAction)
          }
        
        let photoLibrayAction = UIAlertAction(title: "Photo from Libray", style: .Default){ (action) in
            self.showImagePickerController(.PhotoLibrary)
        }
        
        alertController.addAction(photoLibrayAction)
        
        viewController.presentViewController(alertController, animated: true, completion: nil)
    
    }
    
    
    func showImagePickerController(sourceType: UIImagePickerControllerSourceType){
        
       // display a photo taking overlay - or will show the users photo library.
       //        case PhotoLibrary
       //        case Camera
       //        case SavedPhotosAlbum

        
        
        //create a controller in code
        imagePickerController=UIImagePickerController()
        imagePickerController!.sourceType=sourceType
        imagePickerController!.delegate=self
        
        
        self.viewController.presentViewController(imagePickerController!, animated: true, completion: nil)
    }
    
   
}

extension PhotoTakingHelper: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
        callback(image)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        viewController.dismissViewControllerAnimated(true, completion: nil)
    }
}
