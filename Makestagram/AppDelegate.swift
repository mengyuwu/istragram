//
//  AppDelegate.swift
//  Makestagram
//
//  Created by Benjamin Encz on 5/15/15.
//  Copyright (c) 2015 Make School. All rights reserved.
//

import UIKit
import Parse

import FBSDKCoreKit
import ParseUI

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

    var parseLoginHelper: ParseLoginHelper!
    
    override init(){
        super.init()
        //initialize: ParseLoginHelper(callback:) , pass in the varibale name,not the value
        //user,eror will be type PFUser, NSError
        // why need [unowned self]
        
        
        parseLoginHelper = ParseLoginHelper {[unowned self] user, error in
         // initialize the parseloginhelper with a callback
            if let error = error {
                // 1
                ErrorHandling.defaultErrorHandler(error)
            }else if let user=user{
                
                // if login was successful, display the tarbarcontroller
                let storyboard=UIStoryboard(name: "Main", bundle: nil)
                let tarBarController = storyboard.instantiateViewControllerWithIdentifier("TabBarController") as! UIViewController
                
                //We can choose the main View Controller of our app, in code, by setting the rootViewController property of the AppDelegate's window. 
                self.window?.rootViewController!.presentViewController(tarBarController, animated: true, completion: nil)
                
            }
        
        }
    }

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    // Override point for customization after application launch.
    
    Parse.setApplicationId("WCk9JfQ1XhlyLzVJFtZG7WFSsA2jOI0Vt2bzm4Gc", clientKey: "86zql1RUEw4cD3SQWqGa5bW2iNEIE3QSpecvudOm")
    
//    PFUser.logInWithUsername("test", password: "test")
//    
//    if let user=PFUser.currentUser(){
//        println("log in ")
//    }else {
//        println("not logged")
//    }
    
    // change default ACL
    
    let acl = PFACL()
    acl.setPublicReadAccess(true)
    PFACL.setDefaultACL(acl, withAccessForCurrentUser: true)
    
    // Initialize Facebook
    PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions)
    
    let user = PFUser.currentUser()
    
    let startViewController: UIViewController; //? ;
    
    if (user != nil) {
        // if we have a user, set the TabBarControler to be the initial View Controller
        let storyborad=UIStoryboard(name: "Main", bundle: nil) // why Main
        startViewController=storyborad.instantiateViewControllerWithIdentifier("TabBarController") as! UITabBarController
        
    }else {
        // string
    
        // otherwise set the LoginViewContoller to be the first
        let loginViewController = PFLogInViewController()
        loginViewController.fields = .UsernameAndPassword | .LogInButton | .SignUpButton | .PasswordForgotten | .Facebook
        loginViewController.delegate = parseLoginHelper
        loginViewController.signUpController?.delegate=parseLoginHelper
        startViewController=loginViewController
    
    }
    
    self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
    self.window?.rootViewController=startViewController
    self.window?.makeKeyAndVisible()
    
    
    return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    
    //MARK: Facebook Integration
    
    func applicationDidBecomeActive(application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    

  func applicationWillResignActive(application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  }

  

  func applicationWillTerminate(application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }


}

