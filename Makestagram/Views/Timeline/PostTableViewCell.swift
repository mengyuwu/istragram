//
//  PostTableViewCell.swift
//  Makestagram
//
//  Created by 吴梦宇 on 6/28/15.
//  Copyright (c) 2015 Make School. All rights reserved.
//

import UIKit
import Bond
import Parse

class PostTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var likesIconImageView: UIImageView!
    
    @IBOutlet weak var likesLabel: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var moreButton: UIButton!
    

    @IBOutlet weak var postImageView: UIImageView!
    
    
    @IBAction func moreButtonTapped(sender: AnyObject) {
    }
    
    @IBAction func likeButtonTapped(sender: AnyObject) {
        post?.toogleLikePost(PFUser.currentUser()!)
        
    }
    
    var likeBond: Bond<[PFUser]?>! // DOES bond has to be the same type as dyanamics type?
    
    
    var post:Post? {
        didSet {
            
            //oldValue is aviable automatically in the didSet, it allow to access the previous value of a property
            
            if let oldValue = oldValue where oldValue != post {
                
                //println("oldvalue")
                
                likeBond.unbindAll()
                postImageView.designatedBond.unbindAll()
                
                if(oldValue.image.bonds.count == 0){
                    oldValue.image.value=nil
                }
                
            }
            
            
            if let post = post{
                
                // bind the image of the post to the postImage view
            post.image ->> postImageView
            
            post.likes ->> likeBond
                
            }
            
        }
    }
    
  // As soon as we initialize the Bond, we need to provide a closure with all of the code that will run whenever our Bond receives a new value.
    required init(coder aDecoder: NSCoder){
        
        super.init(coder: aDecoder)
        
        //1. bond value initialize , // This way we would have retain cycle in which two objects reference each other strongly. , the cell -> bond, bond closure-> the cell (if not using unownned self)
        
        // unowned keyword similar to the weak keyword, so the cell is not strong reference to the bond
        //The Bond receives the list of users that have liked a post in the likeList parameter.
        
        
        likeBond = Bond<[PFUser]?>(){ [unowned self] likeList in // capture list, avoid retain cycles
            
            
            if let likeList=likeList{
                
              
                
                self.likesLabel.text = self.stringFromUserList(likeList)
            
                
                //bug!
                // when working with PFObjects, two variables are equal when they are recerencing exactly same object
                
               // however, every time we retrieve a post or a user through a PFQuery, a new object is created
                // that new object is not the same as objects that we have received from previous queries
                
                // objects equal whenever they reference the same object on the server, when they have same objectID
                
                
                self.likeButton.selected = contains(likeList, PFUser.currentUser()!)
                
                self.likesIconImageView.hidden = (likeList.count == 0)
                
            }else {
                
                self.likesLabel.text=""
                self.likeButton.selected=false
                self.likesIconImageView.hidden=true
                
                
            }
            
        }
        
        
    }
    
    
    func stringFromUserList(userList: [PFUser]) -> String{
        
        //1 user is one element of usernameList, map user.username to each usernameList
        // mapping from PFUser objects to the usernames of these PFObjects.
        
        //println("userList \(userList) ")
        
        let usernameList = userList.map{ user in
            user.username!
        }
        
        //println("usernameList \(usernameList) ")
        
        let commaSeparatedUserList = ", ".join(usernameList)// sepearte the array using ","
        
        return commaSeparatedUserList
        
    }



    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension PFObject : Equatable {
    
}

public func ==(lhs: PFObject, rhs: PFObject) -> Bool {
    
    return lhs.objectId == rhs.objectId
}
