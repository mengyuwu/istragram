//
//  FriendSearchTableViewCell.swift
//  Makestagram
//
//  Created by 吴梦宇 on 7/1/15.
//  Copyright (c) 2015 Make School. All rights reserved.
//

import UIKit
import Parse

//? why need :class?, only class could extend the delegate but struct can't
protocol FriendSearchTableViewCellDelegate: class {
    func cell(cell: FriendSearchTableViewCell, didSelectFollowUser user: PFUser)
    func cell(cell: FriendSearchTableViewCell, didSelectUnfollowUser user: PFUser)
}



class FriendSearchTableViewCell: UITableViewCell {
    
    // The main features of that cell are displaying a username and a follow button. That follow button can indicate whether or not we are already following a user.we want to keep more complex functionality outside of our views.
    
    //The delegate of each cell will be the FriendSearchViewController. When the follow button is tapped, the FriendTableViewCell will inform its delegate.
    

    
    @IBOutlet weak var usernameLabel: UILabel!
    
    @IBOutlet weak var followButton: UIButton!
    
    weak var delegate : FriendSearchTableViewCellDelegate?
    
    var user: PFUser?{
        didSet{
            usernameLabel.text = user?.username
            
        }
    }
    
    var canFollow: Bool? = true {
        
        didSet{
            /*
            Change the state of the follow button based on whether or not
            it is possible to follow a user.
            */
            if let canFollow = canFollow {
            followButton.selected = !canFollow // canfollow means that it is not selected
            }
        }
        
    }
    
    @IBAction func followButtonTapped(sender: AnyObject) {
        //? where , something like &&
        if let canFollow = canFollow where canFollow == true {
        
            delegate?.cell(self, didSelectFollowUser: user!)
            
            self.canFollow=false
            
        }else {
            
            delegate?.cell(self, didSelectUnfollowUser: user!)
            self.canFollow = true
        }
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
