//
//  Post.swift
//  Makestagram
//
//  Created by 吴梦宇 on 6/28/15.
//  Copyright (c) 2015 Make School. All rights reserved.
//

import Foundation
import Parse
import Bond
import ConvenienceKit

//Parse framework will be able to use that class as the native class for a Parse cloud object.

class Post: PFObject, PFSubclassing {
    
    @NSManaged var imageFile: PFFile?
    @NSManaged var user: PFUser?
    
   // var image: UIImage?
    //That wrapper allows us to listen for changes to the wrapped value. The Dynamic wrapper enables us to use the property together with bindings.
    // we need to append .value to access the value wrapped by the Dynamic
     var image: Dynamic<UIImage?> = Dynamic(nil)
     var likes = Dynamic<[PFUser]?>(nil)
    
    // define static var imageCache so that imageCache can be accessed without create an instance, you can get it from Post.imageCache
    
    //. We define it as static, because the cache does not belong to a particular instance of the Post class, but is instead shared between all posts.
    
    static var imageCache:NSCacheSwift<String,UIImage>!
    
    var photoUploadTask: UIBackgroundTaskIdentifier?
    
    //MARK: PFSubcalssing Protocol
    
    static func parseClassName() -> String {
        return "Post"
    }
    
    override init(){
        super.init()
    }
    
    override class func initialize(){
        
        var onceToken : dispatch_once_t = 0 ;
        dispatch_once(&onceToken){
            // inform parse about this subclass
            self.registerSubclass()
            // 1
            Post.imageCache = NSCacheSwift<String, UIImage>()
        }
        
    }
    
    
    func downloadImage(){
        
        // put in cache
        image.value = Post.imageCache[self.imageFile!.name]
        
        
        if(image.value == nil){
            
            imageFile?.getDataInBackgroundWithBlock{(data: NSData?, error: NSError?) -> Void in
                
                if let error = error {
                    ErrorHandling.defaultErrorHandler(error)
                }

                if let data = data {
                    
                    let image = UIImage(data:data, scale: 1.0)!
                    
                    self.image.value=image
                    
                    Post.imageCache[self.imageFile!.name]=image
                    
                }
                
                
            }
            
            
        }
    }
    
    
    func uploadPost(){
        let imageData=UIImageJPEGRepresentation(image.value, 0.8)
        let imageFile=PFFile(data: imageData)
        
        //1. create a background task,Marks the beginning of a new long-running background task.
photoUploadTask = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler{() -> Void in
           //A handler to be called shortly before the app’s remaining background time reaches 0.
            // when the time is expired, shoudl also delete this task
            //Marks the end of a specific long-running background task.
            UIApplication.sharedApplication().endBackgroundTask(self.photoUploadTask!)
        }
        
        
        imageFile.saveInBackgroundWithBlock{
            (success: Bool, error: NSError? ) -> Void in
            
            if let error = error {
                ErrorHandling.defaultErrorHandler(error)
            }

            // when imageFile is saved successfully we can delete this task
            UIApplication.sharedApplication().endBackgroundTask(self.photoUploadTask!)
        }
        
        self.imageFile=imageFile
        
        self.user = PFUser.currentUser()
        
        saveInBackgroundWithBlock(ErrorHandling.errorHandlingCallback)
        
    }
    
    func fetchLikes(){
        if(likes.value != nil ){
            return
        }

        // second parameter is a call back function
ParseHelper.likesForPost(self, completionBlock:{ (var likes:[AnyObject]?, error: NSError? )  -> Void in

    if let error = error {
        ErrorHandling.defaultErrorHandler(error)
    }

    
    //The filter method takes a closure and returns an array that only contains the objects from the original array that meet the requirement stated in that closure. The closure passed to the filter method gets called for each element in the array, each time passing the current element as the like argument to the closure.
    
    // likes is actually whole likes query results
    likes=likes?.filter { like in like[ParseHelper.ParseLikeFromUser] != nil}
    
    
    
    //unlike filter, map does not remove objects but replaces them. In this particular case, we are replacing the likes in the array with the users that are associated with the like. We start with an array of likes and retrieve an array of users. Then we assign the result to our likes.value property.
   
    // self.likes is [PFUser] type
    self.likes.value = likes?.map{like in
    
         let like=like as! PFObject
          let fromUser = like[ParseHelper.ParseLikeFromUser] as! PFUser
        
         return fromUser

        }


   })
        
  }
    
    func doesUserLikePost(user: PFUser) -> Bool {
        
        if let likes = likes.value {
            
            return contains(likes, user)
            
        }else{
            
            return false
        }
        
        
    }
    
    
    func toogleLikePost(user: PFUser){
        
        if(doesUserLikePost(user)){
            
            //$0 each elements in squence
            likes.value = likes.value?.filter{$0 != user} // local caches
            
            // then syncing the change with parse
            ParseHelper.unlikePost( user, post: self)  //? why need post label
            
       
        }else {
            
            likes.value?.append(user) //add to local cache
            ParseHelper.likePost(user, post: self)// synch the change with Parse
        }
        
        
        
    }
    
    
    
}
